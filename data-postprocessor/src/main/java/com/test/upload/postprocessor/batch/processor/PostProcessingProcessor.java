package com.test.upload.postprocessor.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.test.upload.postprocessor.batch.model.UploadBean;

import io.micrometer.core.instrument.util.StringUtils;

public class PostProcessingProcessor implements ItemProcessor<UploadBean, UploadBean> {
	
	private static final Logger logger = LoggerFactory.getLogger(PostProcessingProcessor.class);

	@Override
	public UploadBean process(UploadBean uploadBean) throws Exception {

		if(uploadBean != null && StringUtils.isNotBlank(uploadBean.getInputData())) {
			
			logger.info("The patient is post processing which is read from Kafka: {}", uploadBean.getInputData());
			uploadBean.setOutputData(uploadBean.getInputDataFromFlow() + "_PostProcessed");
			uploadBean.setUploadStatus("Completed");
			uploadBean.setInputDataFromFlow(null);
			return uploadBean;
			
		}
		else
			logger.info("No patient read from Kafka");
		
		return null;
		
	}

}
