package com.test.upload.postprocessor.batch.connector;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import com.test.upload.postprocessor.batch.dao.PostProcessingDao;
import com.test.upload.postprocessor.batch.dao.UploadDao;
import com.test.upload.postprocessor.batch.model.PostProcessingBean;
import com.test.upload.postprocessor.batch.model.UploadBean;

import io.micrometer.core.instrument.util.StringUtils;

@Component
public class KafkaMessageConsumer implements ItemReader<UploadBean> {

	private Consumer<Long, String> consumer;
	
	@Autowired
	private PostProcessingDao postprocessingDao;
	
	@Autowired
	private UploadDao uploadDao;
	
	private String topic;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaMessageConsumer.class);
	
	public KafkaMessageConsumer(ApplicationArguments args) {

		topic = String.format("%s_PostProcessor", args.getOptionValues("topic").get(0));
		
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, topic);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

		consumer = new KafkaConsumer<>(props);
		
		consumer.subscribe(Collections.singletonList(topic));

	}
	
	@Override
	public UploadBean read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		logger.info("Reading message from Kafka for topic {}", topic);
		
		UploadBean uploadBean = null;
		
		ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofSeconds(1));
		if(consumerRecords != null && !consumerRecords.isEmpty()) {
			uploadBean = new UploadBean(topic.split("_")[1], consumerRecords.iterator().next().value());
			UploadBean dbBean = uploadDao.getUploadBeansFromId(uploadBean.getUploadId());
			uploadBean.set_id(dbBean.get_id());
			uploadBean.setInputData(dbBean.getInputData());
			uploadBean.setFacilityId(dbBean.getFacilityId());
		}
		else if(!allMessagesPushedForPostProcessing()) 
			uploadBean = new UploadBean();
		else {
			consumerRecords = consumer.poll(Duration.ofSeconds(1));
			if(consumerRecords != null && !consumerRecords.isEmpty()) {
				uploadBean = new UploadBean(topic.split("_")[1], consumerRecords.iterator().next().value());
				UploadBean dbBean = uploadDao.getUploadBeansFromId(uploadBean.getUploadId());
				uploadBean.set_id(dbBean.get_id());
				uploadBean.setInputData(dbBean.getInputData());
				uploadBean.setFacilityId(dbBean.getFacilityId());
			}
		}
			
		if(uploadBean != null && StringUtils.isNotBlank(uploadBean.getInputDataFromFlow())) {
			
			logger.info("Read message {} from Kafka for topic {}", uploadBean.getInputDataFromFlow(), topic);
			
		} else {
			
			logger.info("No message read by the consumer for topic {}", topic);
			
		}
		
		return uploadBean;
		
	}
	
	private boolean allMessagesPushedForPostProcessing() {
		
		boolean allMessagesPushedForPostProcessing = true;
		
		String uploadId = topic.split("_")[1];
		PostProcessingBean uploadBean = postprocessingDao.getUploadBeansFromId(uploadId);
		if(uploadBean != null)
			allMessagesPushedForPostProcessing = uploadBean.isAllMessagesPushedForPostProcessing();
		logger.info("allMessagesPushedForProcessing is {} for topic {}", allMessagesPushedForPostProcessing, topic);
		
		return allMessagesPushedForPostProcessing;
		
	}
	
	public void closeConsumerOnBatchCompletion() {
		
		postprocessingDao.delete(postprocessingDao.getUploadBeansFromId(topic.split("_")[1]));
		consumer.close();
		
	}

}
