package com.test.upload.postprocessor.batch.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;

@Configuration
public class MongoDBConfig {

	@Autowired
    private Environment env;
	
	@Bean
    public MongoDbFactory mongoDbFactory() {
		
        return new SimpleMongoClientDbFactory(env.getProperty("spring.data.mongodb.uri"));
    }

	@Bean
	public MongoTemplate mongoTemplate() {
		
		return new MongoTemplate(mongoDbFactory());

    }
	
}
