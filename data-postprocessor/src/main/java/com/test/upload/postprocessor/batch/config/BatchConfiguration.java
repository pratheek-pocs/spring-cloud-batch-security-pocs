package com.test.upload.postprocessor.batch.config;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.test.upload.postprocessor.batch.connector.KafkaMessageConsumer;
import com.test.upload.postprocessor.batch.model.UploadBean;
import com.test.upload.postprocessor.batch.processor.PostProcessingProcessor;

@Configuration
public class BatchConfiguration {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private KafkaMessageConsumer kafkaConsumer;
	
	@Bean
	public Step postprocessingStep(ItemWriter<UploadBean> mongoWriter) {

		return stepBuilderFactory.get("postprocessingStep")
				.<UploadBean, UploadBean> chunk(1)
				.reader(kafkaConsumer)
				.processor(new PostProcessingProcessor())
				.writer(mongoWriter)
				.build();

	}
	
	@Bean
	public ItemWriter<UploadBean> mongoWriter(MongoTemplate mongoTemplate){
		
		MongoItemWriter<UploadBean> writer = new MongoItemWriter<UploadBean>();
        writer.setTemplate(mongoTemplate);
        return writer;
		
	}
	
	@Bean
	public Job postprocessingJob(Step postprocessingStep, JobExecutionListener jobCompletionListener) {

		return jobBuilderFactory.get("postprocessingJob")
				.incrementer(new RunIdIncrementer())
				.start(postprocessingStep)
				.listener(jobCompletionListener)
				.build();
	}
	
	@Bean
	public JobExecutionListener jobCompletionListener(KafkaMessageConsumer kafkaConsumer) {

		JobExecutionListener listener = new JobExecutionListener() {

			@Override
			public void beforeJob(JobExecution jobExecution) {

				// Do nothing

			}

			@Override
			public void afterJob(JobExecution jobExecution) {

				if(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED)) {

					if(kafkaConsumer != null)
						kafkaConsumer.closeConsumerOnBatchCompletion();

				}


			}
		};

		return listener;

	}
	
}
	
