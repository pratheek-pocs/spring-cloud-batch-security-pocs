package com.test.upload.postprocessor.batch.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("upload_test_uploadConfig")
public class UploadBean {

	@Id
	private String _id;
	
	private String uploadId;
	
	private String uploadStatus;
	
	private String facilityId;
	
	private String inputData;
	
	private String outputData;
	
	private String inputDataFromFlow;
	
	public UploadBean() {
		
	}
	
	public UploadBean(String uploadId, String inputDataFromFlow) {
		
		this.uploadId = uploadId;
		this.inputDataFromFlow = inputDataFromFlow;
		
	}
	
	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

	public String getUploadStatus() {
		return uploadStatus;
	}

	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}
	
	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}
	
	public String getInputData() {
		return inputData;
	}

	public void setInputData(String inputData) {
		this.inputData = inputData;
	}

	public String getOutputData() {
		return outputData;
	}

	public void setOutputData(String outputData) {
		this.outputData = outputData;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getInputDataFromFlow() {
		return inputDataFromFlow;
	}

	public void setInputDataFromFlow(String inputDataFromFlow) {
		this.inputDataFromFlow = inputDataFromFlow;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
		result = prime * result + ((inputData == null) ? 0 : inputData.hashCode());
		result = prime * result + ((inputDataFromFlow == null) ? 0 : inputDataFromFlow.hashCode());
		result = prime * result + ((outputData == null) ? 0 : outputData.hashCode());
		result = prime * result + ((uploadId == null) ? 0 : uploadId.hashCode());
		result = prime * result + ((uploadStatus == null) ? 0 : uploadStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadBean other = (UploadBean) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		if (facilityId == null) {
			if (other.facilityId != null)
				return false;
		} else if (!facilityId.equals(other.facilityId))
			return false;
		if (inputData == null) {
			if (other.inputData != null)
				return false;
		} else if (!inputData.equals(other.inputData))
			return false;
		if (inputDataFromFlow == null) {
			if (other.inputDataFromFlow != null)
				return false;
		} else if (!inputDataFromFlow.equals(other.inputDataFromFlow))
			return false;
		if (outputData == null) {
			if (other.outputData != null)
				return false;
		} else if (!outputData.equals(other.outputData))
			return false;
		if (uploadId == null) {
			if (other.uploadId != null)
				return false;
		} else if (!uploadId.equals(other.uploadId))
			return false;
		if (uploadStatus == null) {
			if (other.uploadStatus != null)
				return false;
		} else if (!uploadStatus.equals(other.uploadStatus))
			return false;
		return true;
	}

}
