package com.test.upload.postprocessor.batch.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.upload.postprocessor.batch.model.UploadBean;

@Repository
public interface UploadDao extends MongoRepository<UploadBean, String> {
	
	@Query( "{'uploadId': ?0}, $limit:1 " )
	public UploadBean getUploadBeansFromId(String uploadId);
	
}
