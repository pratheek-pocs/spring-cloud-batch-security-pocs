package com.test.upload.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

@Configuration
@EnableWebSecurity
public class AppConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private APIExceptionHandler exceptionHandler;
	
	@Autowired
	private AuthenticationProvider authenticationProvider;
	
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) {

		auth.authenticationProvider(authenticationProvider);

	}
	
	@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

		AuthenticationFilter authenticationFilter = authenticationFilter();
		
		httpSecurity
        .antMatcher("/*")
        .csrf()
        	.disable()
        .sessionManagement()
        	.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling()
        .and()
        .authenticationProvider(authenticationProvider)
        	.addFilterBefore(authenticationFilter, BasicAuthenticationFilter.class)
        .authorizeRequests()
        	.anyRequest()
        	.authenticated();
        
    }
	
	private AuthenticationFilter authenticationFilter() throws Exception {
		
		final AuthenticationFilter filter = new AuthenticationFilter(new OrRequestMatcher(new AntPathRequestMatcher("/*")));
		filter.setAuthenticationManager(authenticationManager());
		filter.setAuthenticationFailureHandler(exceptionHandler);
		//filter.setAuthenticationSuccessHandler(successHandler());
		return filter;
		
	}

}
