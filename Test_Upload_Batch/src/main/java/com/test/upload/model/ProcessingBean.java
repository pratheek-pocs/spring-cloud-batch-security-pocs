package com.test.upload.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("upload_test_processingConfig")
public class ProcessingBean {

	@Id
	private String _id;
	
	private String uploadId;
	
	private boolean allMessagesPushedForProcessing;
	
	public ProcessingBean(String uploadId, boolean allMessagesPushedForProcessing) {
		super();
		this.uploadId = uploadId;
		this.allMessagesPushedForProcessing = allMessagesPushedForProcessing;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

	public boolean isAllMessagesPushedForProcessing() {
		return allMessagesPushedForProcessing;
	}

	public void setAllMessagesPushedForProcessing(boolean allMessagesPushedForProcessing) {
		this.allMessagesPushedForProcessing = allMessagesPushedForProcessing;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result + (allMessagesPushedForProcessing ? 1231 : 1237);
		result = prime * result + ((uploadId == null) ? 0 : uploadId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcessingBean other = (ProcessingBean) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		if (allMessagesPushedForProcessing != other.allMessagesPushedForProcessing)
			return false;
		if (uploadId == null) {
			if (other.uploadId != null)
				return false;
		} else if (!uploadId.equals(other.uploadId))
			return false;
		return true;
	}

}
