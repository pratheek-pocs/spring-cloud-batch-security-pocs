package com.test.upload.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.upload.connector.KafkaProducerConnector;
import com.test.upload.dao.ConversionDao;
import com.test.upload.dao.PostProcessingDao;
import com.test.upload.dao.PreprocessingDao;
import com.test.upload.dao.ProcessingDao;
import com.test.upload.dao.UploadDao;
import com.test.upload.model.ConversionBean;
import com.test.upload.model.PostProcessingBean;
import com.test.upload.model.PreprocessingBean;
import com.test.upload.model.ProcessingBean;
import com.test.upload.model.UploadBean;

@RestController
public class FileUploadController {

	@Autowired
	private UploadDao uploadDao;
	
	@Autowired
	private ConversionDao conversionDao;
	
	@Autowired
	private PreprocessingDao preprocessingDao;
	
	@Autowired
	private ProcessingDao processingDao;
	
	@Autowired
	private PostProcessingDao postProcessingDao;
	
	@PostMapping("/upload")
	public ResponseEntity<String> uploadData(@RequestBody UploadBean uploadBean) throws Exception {
		
		uploadDao.save(uploadBean);
		conversionDao.save(new ConversionBean(uploadBean.getUploadId(), true));
		preprocessingDao.save(new PreprocessingBean(uploadBean.getUploadId(), false));
		processingDao.save(new ProcessingBean(uploadBean.getUploadId(), false));
		postProcessingDao.save(new PostProcessingBean(uploadBean.getUploadId(), false));
		
		KafkaProducerConnector kafkaProducer = new KafkaProducerConnector(String.format("%s_%s", uploadBean.getFacilityId(), uploadBean.getUploadId()));
		kafkaProducer.pushMessage(uploadBean.getInputData());
		kafkaProducer.getProducer().close();
		
		return new ResponseEntity<>("Success", HttpStatus.OK);
		
	}
	
}
