package com.test.upload.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.test.upload.model.UploadBean;

@Repository
public interface UploadDao extends MongoRepository<UploadBean, String> {
	
	
}
