package com.test.upload.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.test.upload.model.ConversionBean;

@Repository
public interface ConversionDao extends MongoRepository<ConversionBean, String> {
	
	
}
