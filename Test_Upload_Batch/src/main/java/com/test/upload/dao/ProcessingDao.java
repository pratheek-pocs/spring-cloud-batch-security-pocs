package com.test.upload.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.test.upload.model.ProcessingBean;

@Repository
public interface ProcessingDao extends MongoRepository<ProcessingBean, String> {
	
	
}
