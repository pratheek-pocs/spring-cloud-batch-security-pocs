package com.test.upload.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.test.upload.model.PreprocessingBean;

@Repository
public interface PreprocessingDao extends MongoRepository<PreprocessingBean, String> {
	
	
}
