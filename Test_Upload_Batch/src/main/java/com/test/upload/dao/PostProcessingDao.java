package com.test.upload.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.test.upload.model.PostProcessingBean;

@Repository
public interface PostProcessingDao extends MongoRepository<PostProcessingBean, String> {
	
	
}
