package com.test.upload.connector;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaProducerConnector {

	private KafkaProducer<Long, String> producer;
	private String topic;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerConnector.class);
	
	public KafkaProducerConnector(String topic) {
		
		topic = String.format("%s_Conversion", topic);
		
		Properties properties = new Properties();
		
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.CLIENT_ID_CONFIG, topic);
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		this.producer = new KafkaProducer<Long, String>(properties);
		this.topic = topic;
		
	}
	
	
	public void pushMessage(String inputMessage) throws Exception {

		logger.info("Before writing message to kafka for topic {}", topic);

		producer.send( new ProducerRecord<Long, String>(topic, Long.getLong("1"), inputMessage) );

		logger.info("After writing message {} to kafka", inputMessage);

	}

	public KafkaProducer<Long, String> getProducer() {
		return producer;
	}

}
