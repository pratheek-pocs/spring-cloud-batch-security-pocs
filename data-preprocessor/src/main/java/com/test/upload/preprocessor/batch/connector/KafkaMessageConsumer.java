package com.test.upload.preprocessor.batch.connector;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import com.test.upload.preprocessor.batch.dao.PreprocessingDao;
import com.test.upload.preprocessor.batch.dao.ProcessingDao;
import com.test.upload.preprocessor.batch.model.PreprocessingBean;
import com.test.upload.preprocessor.batch.model.ProcessingBean;

import io.micrometer.core.instrument.util.StringUtils;

@Component
public class KafkaMessageConsumer implements ItemReader<String> {

	private Consumer<Long, String> consumer;
	
	@Autowired
	private PreprocessingDao preprocessingDao;
	
	@Autowired
	private ProcessingDao processingDao;
	
	private String topic;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaMessageConsumer.class);
	
	public KafkaMessageConsumer(ApplicationArguments args) {

		topic = String.format("%s_Preprocessor", args.getOptionValues("topic").get(0));
		
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, topic);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

		consumer = new KafkaConsumer<>(props);
		
		consumer.subscribe(Collections.singletonList(topic));

	}
	
	@Override
	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		logger.info("Reading message from Kafka for topic {}", topic);
		
		String conversionMsg = null;
		
		ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofSeconds(1));
		if(consumerRecords != null && !consumerRecords.isEmpty())
			conversionMsg = consumerRecords.iterator().next().value();
		else if(!allMessagesPushedForPreprocessing()) 
			conversionMsg = "";
		else {
			consumerRecords = consumer.poll(Duration.ofSeconds(1));
			if(consumerRecords != null && !consumerRecords.isEmpty())
				conversionMsg = consumerRecords.iterator().next().value();
		}
			
		if(StringUtils.isNotBlank(conversionMsg)) {
			
			logger.info("Read message {} from Kafka for topic {}", conversionMsg, topic);
			
		} else {
			
			logger.info("No message read by the consumer for topic {}", topic);
			
		}
		
		return conversionMsg;
		
	}
	
	private boolean allMessagesPushedForPreprocessing() {
		
		boolean allMessagesPushedForPreprocessing = true;
		
		String uploadId = topic.split("_")[1];
		PreprocessingBean uploadBean = preprocessingDao.getUploadBeansFromId(uploadId);
		if(uploadBean != null)
			allMessagesPushedForPreprocessing = uploadBean.isAllMessagesPushedForPreprocessing();
		logger.info("allMessagesPushedForPreprocessing is {} for topic {}", allMessagesPushedForPreprocessing, topic);
		
		return allMessagesPushedForPreprocessing;
		
	}

	public void closeConsumerOnBatchCompletion() {
		
		updateUploadBean();
		consumer.close();
		
	}

	private void updateUploadBean() {
		
		ProcessingBean uploadBean = processingDao.getUploadBeansFromId(topic.split("_")[1]);
		uploadBean.setAllMessagesPushedForProcessing(true);
		processingDao.save(uploadBean);
		
		PreprocessingBean preprocessingBean = preprocessingDao.getUploadBeansFromId(topic.split("_")[1]);
		if(preprocessingBean != null)
			preprocessingDao.delete(preprocessingBean);
		
	}
	
	public Consumer<Long, String> getConsumer(){
		
		return consumer;
		
	}

}
