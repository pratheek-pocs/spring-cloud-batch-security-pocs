package com.test.upload.preprocessor.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import io.micrometer.core.instrument.util.StringUtils;

public class PreprocessingProcessor implements ItemProcessor<String, String> {
	
	private static final Logger logger = LoggerFactory.getLogger(PreprocessingProcessor.class);

	@Override
	public String process(String patient) throws Exception {

		
		if(StringUtils.isNotBlank(patient)) {
			
			logger.info("The patient is preprocessing which is read from Kafka: {}", patient);
			return patient + "_PreProcessed";
			
		}
		else
			logger.info("No patient read from Kafka");
		
		return patient;
		
	}

}
