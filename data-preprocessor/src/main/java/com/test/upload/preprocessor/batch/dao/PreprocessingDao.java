package com.test.upload.preprocessor.batch.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.upload.preprocessor.batch.model.PreprocessingBean;

@Repository
public interface PreprocessingDao extends MongoRepository<PreprocessingBean, String> {
	
	@Query( "{'uploadId': ?0}, $limit:1 " )
	public PreprocessingBean getUploadBeansFromId(String uploadId);
	
}
