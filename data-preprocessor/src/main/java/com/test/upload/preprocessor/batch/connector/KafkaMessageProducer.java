package com.test.upload.preprocessor.batch.connector;

import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import io.micrometer.core.instrument.util.StringUtils;

@Component
public class KafkaMessageProducer implements ItemWriter<String> {

	private KafkaProducer<Long, String> producer;
	
	private String topic;
	
	@Autowired
	private KafkaMessageConsumer kafkaMessageConsumer;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaMessageProducer.class);
	
	public KafkaMessageProducer(ApplicationArguments args) {
		
		topic = String.format("%s_Processor", args.getOptionValues("topic").get(0));
		
		Properties properties = new Properties();
		
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.CLIENT_ID_CONFIG, topic);
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		this.producer = new KafkaProducer<Long, String>(properties);
		
	}
	
	@Override
	public void write(List<? extends String> items) throws Exception {
		
		logger.info("Before writing message to kafka for topic {}", topic);
		
		if(!CollectionUtils.isEmpty(items) && StringUtils.isNotBlank(items.get(0))) {
			
			producer.send( new ProducerRecord<Long, String>(topic, Long.getLong("1"), items.get(0)) );
			
			kafkaMessageConsumer.getConsumer().commitSync();
			
			logger.info("After writing message {} to kafka", items.get(0));
			
		} else {
			
			logger.info("No message to write to Kafka for topic {}", topic);
			
		}
		
	}

	public KafkaProducer<Long, String> getProducer() {
		return producer;
	}

}
