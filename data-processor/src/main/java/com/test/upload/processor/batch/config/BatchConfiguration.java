package com.test.upload.processor.batch.config;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.test.upload.processor.batch.connector.KafkaMessageConsumer;
import com.test.upload.processor.batch.connector.KafkaMessageProducer;
import com.test.upload.processor.batch.processor.ProcessingProcessor;

@Configuration
public class BatchConfiguration {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private KafkaMessageProducer kafkaProducer;
	
	@Autowired
	private KafkaMessageConsumer kafkaConsumer;
	
	@Bean
	public Step processingStep() {

		return stepBuilderFactory.get("processingStep")
				.<String, String> chunk(1)
				.reader(kafkaConsumer)
				.processor(new ProcessingProcessor())
				.writer(kafkaProducer)
				.build();

	}
	
	@Bean
	public Job processingJob(Step processingStep, JobExecutionListener jobCompletionListener) {

		return jobBuilderFactory.get("processingJob")
				.incrementer(new RunIdIncrementer())
				.start(processingStep)
				.listener(jobCompletionListener)
				.build();
	}
	
	@Bean
	public JobExecutionListener jobCompletionListener(KafkaMessageProducer kafkaProducer, KafkaMessageConsumer kafkaConsumer) {

		JobExecutionListener listener = new JobExecutionListener() {

			@Override
			public void beforeJob(JobExecution jobExecution) {

				// Do nothing

			}

			@Override
			public void afterJob(JobExecution jobExecution) {

				if(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED)) {

					if(kafkaProducer != null)
						kafkaProducer.getProducer().close();

					if(kafkaConsumer != null)
						kafkaConsumer.closeConsumerOnBatchCompletion();

				}


			}
		};

		return listener;

	}
	
}
	
