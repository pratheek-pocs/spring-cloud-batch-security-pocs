package com.test.upload.processor.batch.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.upload.processor.batch.model.ProcessingBean;

@Repository
public interface ProcessingDao extends MongoRepository<ProcessingBean, String> {
	
	@Query( "{'uploadId': ?0}, $limit:1 " )
	public ProcessingBean getUploadBeansFromId(String uploadId);
	
}
