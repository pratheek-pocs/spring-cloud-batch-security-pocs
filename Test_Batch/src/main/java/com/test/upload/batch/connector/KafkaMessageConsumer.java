package com.test.upload.batch.connector;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.test.upload.batch.dao.UploadDao;
import com.test.upload.batch.model.UploadBean;

import io.micrometer.core.instrument.util.StringUtils;

public class KafkaMessageConsumer implements ItemReader<String> {

	private Consumer<Long, String> consumer;
	private String topic;
	
	private UploadDao uploadDao;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaMessageConsumer.class);
	
	public KafkaMessageConsumer(String topic, UploadDao uploadDao) {

		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, topic);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

		consumer = new KafkaConsumer<>(props);
		
		consumer.subscribe(Collections.singletonList(topic));
		this.topic = topic;
		this.uploadDao = uploadDao;

	}
	
	@Override
	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		logger.info("Reading message from Kafka for topic {}", topic);
		
		String conversionMsg = null;
		
		if(!isUploadBatchCompleted()) {
			
			conversionMsg = "";
			
			ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofSeconds(1));
			if(consumerRecords != null && !consumerRecords.isEmpty())
				conversionMsg = consumerRecords.iterator().next().value();
		}
			
		if(StringUtils.isNotBlank(conversionMsg)) {
			
			logger.info("Read message {} from Kafka for topic {}", conversionMsg, topic);
			consumer.commitSync();
			
		} else {
			
			logger.info("No message read by the consumer for topic {}", topic);
			
		}
		
		return conversionMsg;
		
	}
	
	private boolean isUploadBatchCompleted() {
		
		boolean isUploadBatchCompleted = true;
		
		String uploadId = topic.split("_")[1];
		UploadBean uploadBean = uploadDao.getUploadBeansFromId(uploadId);
		if(uploadBean != null)
			isUploadBatchCompleted = uploadBean.isUploadBatchCompleted();
		logger.info("isUploadBatchCompleted is {} for topic {}", isUploadBatchCompleted, topic);
		
		return isUploadBatchCompleted;
		
	}

	public Consumer<Long, String> getConsumer() {
		return consumer;
	}

}
