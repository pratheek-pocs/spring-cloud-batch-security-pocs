package com.test.upload.batch.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.test.upload.batch.connector.KafkaMessageConsumer;
import com.test.upload.batch.connector.KafkaMessageProducer;
import com.test.upload.batch.connector.MongoDBMessageProducer;
import com.test.upload.batch.constants.BatchConfigurationConstants;
import com.test.upload.batch.dao.UploadDao;
import com.test.upload.batch.model.UploadBean;
import com.test.upload.batch.processor.ConversionProcessor;
import com.test.upload.batch.processor.PostProcessingProcessor;
import com.test.upload.batch.processor.PreProcessingProcessor;
import com.test.upload.batch.processor.ProcessingProcessor;

@Component
public class BatchStepsConfiguration {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private UploadDao uploadDao;
	
	public List<Job> getJobsForUploads(List<UploadBean> uploadBeans){
		
		List<Job> jobs = new ArrayList<>();
		
		if(!CollectionUtils.isEmpty(uploadBeans)) 
			uploadBeans.forEach(uploadBean -> {

				String stepName = String.format("%s_%s", uploadBean.getFacilityId(), uploadBean.getUploadId()); 

				KafkaMessageConsumer conversionConsumer = new KafkaMessageConsumer(stepName + BatchConfigurationConstants.CONVERTER, uploadDao);
				KafkaMessageProducer preprocessingProducer = new KafkaMessageProducer(stepName + BatchConfigurationConstants.PREPROCESSOR, null);
				
				KafkaMessageConsumer preprocessingConsumer = new KafkaMessageConsumer(stepName + BatchConfigurationConstants.PREPROCESSOR, uploadDao);
				KafkaMessageProducer processingProducer = new KafkaMessageProducer(stepName + BatchConfigurationConstants.PROCESSOR, null);
				
				KafkaMessageConsumer processingConsumer = new KafkaMessageConsumer(stepName + BatchConfigurationConstants.PROCESSOR, uploadDao);
				KafkaMessageProducer postprocessingProducer = new KafkaMessageProducer(stepName + BatchConfigurationConstants.POSTPROCESSOR, null);
				
				KafkaMessageConsumer postprocessingConsumer = new KafkaMessageConsumer(stepName + BatchConfigurationConstants.POSTPROCESSOR, uploadDao);
				MongoDBMessageProducer afterPostProcessingProducer = new MongoDBMessageProducer(stepName, uploadDao);
				
				jobs.add(jobBuilderFactory.get(stepName + "_ConversionJob").start(
											conversionStep(stepBuilderFactory, stepName + "_ConversionStep", conversionConsumer, preprocessingProducer))
											.listener(jobCompletionListener(preprocessingProducer, conversionConsumer))
											.build());
				
				jobs.add(jobBuilderFactory.get(stepName + "_PreprocessingJob").start(
											preProcessingStep(stepBuilderFactory, stepName + "_PreProcessingStep", preprocessingConsumer, processingProducer))
											.listener(jobCompletionListener(processingProducer, preprocessingConsumer))
											.build());
				
				
				  jobs.add(jobBuilderFactory.get(stepName + "_ProcessingJob").start(
				  processingStep(stepBuilderFactory, stepName + "_ProcessingStep",
				  processingConsumer, postprocessingProducer))
				  .listener(jobCompletionListener(postprocessingProducer, processingConsumer))
				  .build());
				  
				  jobs.add(jobBuilderFactory.get(stepName + "_PostProcessingJob").start(
				  postProcessingStep(stepBuilderFactory, stepName + "_PostProcessingStep",
				  postprocessingConsumer, afterPostProcessingProducer))
				  .listener(jobCompletionListener(null, postprocessingConsumer)) .build());
				 

			});
		
		return jobs;
		
	}
	
	private JobExecutionListener jobCompletionListener(KafkaMessageProducer producer, KafkaMessageConsumer consumer) {
		
		JobExecutionListener listener = new JobExecutionListener() {

            @Override
            public void beforeJob(JobExecution jobExecution) {

                // Do nothing
            	
            }

            @Override
            public void afterJob(JobExecution jobExecution) {

            	if(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED)) {
            		
            		if(producer != null)
            			producer.getProducer().close();
            		
            		if(consumer != null)
            			consumer.getConsumer().close();
            		
            	}
            		
                
            }
        };
		
        return listener;
        
	}
	
	private Step conversionStep(StepBuilderFactory stepBuilderFactory, String stepName, KafkaMessageConsumer consumer, KafkaMessageProducer producer) {

		return stepBuilderFactory.get(stepName)
				.<String, String> chunk(1)
				.reader(consumer)
				.processor(new ConversionProcessor())
				.writer(producer)
				.build();

	}
	
	private Step preProcessingStep(StepBuilderFactory stepBuilderFactory, String stepName, KafkaMessageConsumer consumer, KafkaMessageProducer producer) {

		return stepBuilderFactory.get(stepName)
				.<String, String> chunk(1)
				.reader(consumer)
				.processor(new PreProcessingProcessor())
				.writer(producer)
				.build();

	}
	
	private Step processingStep(StepBuilderFactory stepBuilderFactory, String stepName, KafkaMessageConsumer consumer, KafkaMessageProducer producer) {

		return stepBuilderFactory.get(stepName)
				.<String, String> chunk(1)
				.reader(consumer)
				.processor(new ProcessingProcessor())
				.writer(producer)
				.build();

	}
	
	private Step postProcessingStep(StepBuilderFactory stepBuilderFactory, String stepName, KafkaMessageConsumer consumer, MongoDBMessageProducer producer) {

		return stepBuilderFactory.get(stepName)
				.<String, String> chunk(1)
				.reader(consumer)
				.processor(new PostProcessingProcessor())
				.writer(producer)
				.build();

	}
	
}
