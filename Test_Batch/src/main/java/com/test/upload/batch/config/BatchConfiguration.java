package com.test.upload.batch.config;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import com.test.upload.batch.dao.UploadDao;
import com.test.upload.batch.model.UploadBean;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	private UploadDao uploadDao;
	
	@Autowired
	private BatchStepsConfiguration batchStepsConfig;
	
	@Bean
	public JobLauncher jobLauncher(JobRepository jobRepository) throws Exception {
		
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setJobRepository(jobRepository);
		jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
		jobLauncher.afterPropertiesSet();
		return jobLauncher;
		
	}

	@Bean
	public List<Job> initiatefileUploadJobs() {

		List<UploadBean> uploadBeans = uploadDao.getUploadBeansWithStatus("Processing");
		
		return batchStepsConfig.getJobsForUploads(uploadBeans);

	}
	
}
	
