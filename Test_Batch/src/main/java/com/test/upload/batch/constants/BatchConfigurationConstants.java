package com.test.upload.batch.constants;

public class BatchConfigurationConstants {

	public static final String CONVERTER = "_Converter";
	public static final String PREPROCESSOR = "_Preprocessor";
	public static final String PROCESSOR = "_Processor";
	public static final String POSTPROCESSOR = "_PostProcessor";
	
}
