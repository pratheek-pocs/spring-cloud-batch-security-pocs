package com.test.upload.batch.connector;

import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.util.CollectionUtils;

import com.test.upload.batch.dao.UploadDao;
import com.test.upload.batch.model.UploadBean;

import io.micrometer.core.instrument.util.StringUtils;

public class KafkaMessageProducer implements ItemWriter<String> {

	private KafkaProducer<Long, String> producer;
	private String topic;
	private UploadDao uploadDao;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaMessageProducer.class);
	
	public KafkaMessageProducer(String topic, UploadDao uploadDao) {
		
		Properties properties = new Properties();
		
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.CLIENT_ID_CONFIG, topic);
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		this.producer = new KafkaProducer<Long, String>(properties);
		this.topic = topic;
		this.uploadDao = uploadDao;
		
	}
	
	@Override
	public void write(List<? extends String> items) throws Exception {
		
		logger.info("Before writing message to kafka for topic {}", topic);
		
		if(!CollectionUtils.isEmpty(items) && StringUtils.isNotBlank(items.get(0))) {
			
			if(uploadDao != null) {
				
				UploadBean updatingUploadBean = uploadDao.getUploadBeansFromId(topic.split("_")[1]);
				updatingUploadBean.setOutputData(items.get(0));
				uploadDao.save(updatingUploadBean);
				
			}

			producer.send( new ProducerRecord<Long, String>(topic, Long.getLong("1"), items.get(0)) );
			
			logger.info("After writing message {} to kafka", items.get(0));
			
		} else {
			
			logger.info("No message to write to Kafka for topic {}", topic);
			
		}
		
	}

	public KafkaProducer<Long, String> getProducer() {
		return producer;
	}

}
