package com.test.upload.batch.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.upload.batch.model.UploadBean;

@Repository
public interface UploadDao extends MongoRepository<UploadBean, String> {
	
	@Query( "{'uploadStatus': ?0}" )
	public List<UploadBean> getUploadBeansWithStatus(String uploadStatus);
	
	@Query( "{'uploadId': ?0}, $limit:1 " )
	public UploadBean getUploadBeansFromId(String uploadId);
	
	
}
