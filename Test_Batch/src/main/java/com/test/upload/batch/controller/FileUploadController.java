package com.test.upload.batch.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.upload.batch.config.BatchStepsConfiguration;
import com.test.upload.batch.connector.KafkaMessageProducer;
import com.test.upload.batch.constants.BatchConfigurationConstants;
import com.test.upload.batch.dao.UploadDao;
import com.test.upload.batch.model.UploadBean;

@RestController
public class FileUploadController {

	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private BatchStepsConfiguration batchStepsConfig;
	
	@Autowired
	private UploadDao uploadDao;
	
	@PostMapping("/upload")
	public ResponseEntity<String> uploadData(@RequestBody UploadBean uploadBean) throws Exception {
		
		uploadDao.save(uploadBean);
		
		createKafkaTopicsIfNotThere(uploadBean);
		
		List<Job> jobs = batchStepsConfig.getJobsForUploads(Arrays.asList(uploadBean));
		jobs.forEach(job -> {
			
			try {
				jobLauncher.run(job, new JobParametersBuilder().toJobParameters());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		});
		
		KafkaMessageProducer kafkaProducer = new KafkaMessageProducer(String.format("%s_%s%s", uploadBean.getFacilityId(), uploadBean.getUploadId(), BatchConfigurationConstants.CONVERTER), null);
		kafkaProducer.write(Arrays.asList(uploadBean.getInputData()));
		kafkaProducer.getProducer().close();
		
		return new ResponseEntity<>("Success", HttpStatus.OK);
		
	}
	
	private void createKafkaTopicsIfNotThere(UploadBean uploadBean) throws InterruptedException, ExecutionException {
		
		List<String> topics = Arrays.asList(uploadBean.getFacilityId() + "_" + uploadBean.getUploadId() + BatchConfigurationConstants.CONVERTER, 
											uploadBean.getFacilityId() + "_" + uploadBean.getUploadId() + BatchConfigurationConstants.PREPROCESSOR,
											uploadBean.getFacilityId() + "_" + uploadBean.getUploadId() + BatchConfigurationConstants.PROCESSOR,
											uploadBean.getFacilityId() + "_" + uploadBean.getUploadId() + BatchConfigurationConstants.POSTPROCESSOR);
		
		Collection<NewTopic> newTopics = new ArrayList<>();
		short replicationFactor = 1;
		int numShards = 1;
		
		Properties props = new Properties();
		props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		try (AdminClient client = AdminClient.create(props)) {
			
		    ListTopicsOptions options = new ListTopicsOptions();
		    ListTopicsResult topicsResult = client.listTopics(options);
		    Set<String> currentTopicList = topicsResult.names().get();
		    topics.forEach(topic -> {
		    	
		    	if(!currentTopicList.contains(topic))
		    		newTopics.add(new NewTopic(topic, numShards, replicationFactor));
		    		
		    });
		    
		    client.createTopics(newTopics);
		    
		    
		}
		
	}
	
	
}
