package com.test.upload.batch.connector;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.util.CollectionUtils;

import com.test.upload.batch.dao.UploadDao;
import com.test.upload.batch.model.UploadBean;

import io.micrometer.core.instrument.util.StringUtils;

public class MongoDBMessageProducer implements ItemWriter<String> {

	private static final Logger logger = LoggerFactory.getLogger(MongoDBMessageProducer.class);
	
	private UploadDao uploadDao;
	private String topic;
	
	public MongoDBMessageProducer(String topic, UploadDao uploadDao) {
		
		this.uploadDao = uploadDao;
		this.topic = topic;
		
	}
	
	@Override
	public void write(List<? extends String> items) throws Exception {
		
		logger.info("Before writing message to MongoDB");
		
		if(!CollectionUtils.isEmpty(items) && StringUtils.isNotBlank(items.get(0))) {
			
			String message = items.get(0);
			
			UploadBean updatingUploadBean = uploadDao.getUploadBeansFromId(topic.split("_")[1]);
			updatingUploadBean.setOutputData(message);
			updatingUploadBean.setUploadBatchCompleted(true);
			updatingUploadBean.setUploadStatus("Completed");
			
			uploadDao.save(updatingUploadBean);
			
			logger.info("After writing message {} to MongoDB for uploadId {}", message, topic.split("_")[1]);
			
		} else {
			
			logger.info("No message to write to MongoDB");
			
		}
		
	}
	
}
