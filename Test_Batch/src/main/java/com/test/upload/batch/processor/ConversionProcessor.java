package com.test.upload.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import io.micrometer.core.instrument.util.StringUtils;

public class ConversionProcessor implements ItemProcessor<String, String> {
	
	private static final Logger logger = LoggerFactory.getLogger(ConversionProcessor.class);

	@Override
	public String process(String patient) throws Exception {

		
		if(StringUtils.isNotBlank(patient)) {
			
			logger.info("The patient is processing which is read from Kafka: {}", patient);
			return patient + "_Converted";
			
		}
		else
			logger.info("No patient read from Kafka");
		
		return patient;
		
	}

}
