package com.test.upload.conversion.batch.config;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.test.upload.conversion.batch.connector.KafkaMessageConsumer;
import com.test.upload.conversion.batch.connector.KafkaMessageProducer;
import com.test.upload.conversion.batch.processor.ConversionProcessor;

@Configuration
public class BatchConfiguration {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private KafkaMessageProducer kafkaProducer;
	
	@Autowired
	private KafkaMessageConsumer kafkaConsumer;
	
	@Bean
	public Step conversionStep() {

		return stepBuilderFactory.get("conversionStep")
				.<String, String> chunk(1)
				.reader(kafkaConsumer)
				.processor(new ConversionProcessor())
				.writer(kafkaProducer)
				.build();

	}
	
	@Bean
	public Job conversionJob(Step conversionStep, JobExecutionListener jobCompletionListener) {

		return jobBuilderFactory.get("conversionJob")
				.incrementer(new RunIdIncrementer())
				.start(conversionStep)
				.listener(jobCompletionListener)
				.build();
	}
	
	@Bean
	public JobExecutionListener jobCompletionListener(KafkaMessageProducer kafkaProducer, KafkaMessageConsumer kafkaConsumer) {

		JobExecutionListener listener = new JobExecutionListener() {

			@Override
			public void beforeJob(JobExecution jobExecution) {

				// Do nothing

			}

			@Override
			public void afterJob(JobExecution jobExecution) {

				if(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED)) {

					if(kafkaProducer != null)
						kafkaProducer.getProducer().close();

					if(kafkaConsumer != null)
						kafkaConsumer.closeConsumerOnBatchCompletion();

				}


			}
		};

		return listener;

	}
	
}
	
