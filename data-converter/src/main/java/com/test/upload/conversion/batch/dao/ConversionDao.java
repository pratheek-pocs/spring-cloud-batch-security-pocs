package com.test.upload.conversion.batch.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.upload.conversion.batch.model.ConversionBean;

@Repository
public interface ConversionDao extends MongoRepository<ConversionBean, String> {
	
	@Query( "{'uploadId': ?0}, $limit:1 " )
	public ConversionBean getUploadBeansFromId(String uploadId);
	
}
