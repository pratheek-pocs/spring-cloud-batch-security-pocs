package com.test.upload.conversion.batch.connector;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import com.test.upload.conversion.batch.dao.ConversionDao;
import com.test.upload.conversion.batch.dao.PreprocessingDao;
import com.test.upload.conversion.batch.model.ConversionBean;
import com.test.upload.conversion.batch.model.PreprocessingBean;

import io.micrometer.core.instrument.util.StringUtils;

@Component
public class KafkaMessageConsumer implements ItemReader<String> {

	private Consumer<Long, String> consumer;
	
	@Autowired
	private ConversionDao conversionDao;
	
	@Autowired
	private PreprocessingDao preprocessingDao;
	
	private String topic;
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaMessageConsumer.class);
	
	public KafkaMessageConsumer(ApplicationArguments args) {

		topic = String.format("%s_Conversion", args.getOptionValues("topic").get(0));
		
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, topic);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

		consumer = new KafkaConsumer<>(props);
		
		consumer.subscribe(Collections.singletonList(topic));
		consumer.commitSync();

	}
	
	@Override
	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		logger.info("Reading message from Kafka for topic {}", topic);
		
		String conversionMsg = null;
		
		ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofSeconds(1));
		if(consumerRecords != null && !consumerRecords.isEmpty())
			conversionMsg = consumerRecords.iterator().next().value();
		else if(!allMessagesPushedForConversion()) 
			conversionMsg = "";
		else {
			consumerRecords = consumer.poll(Duration.ofSeconds(1));
			if(consumerRecords != null && !consumerRecords.isEmpty())
				conversionMsg = consumerRecords.iterator().next().value();
		}
		
		if(StringUtils.isNotBlank(conversionMsg)) {
			
			logger.info("Read message {} from Kafka for topic {}", conversionMsg, topic);
			
		} else {
			
			logger.info("No message read by the consumer for topic {}", topic);
			
		}
		
		return conversionMsg;
		
	}
	
	private boolean allMessagesPushedForConversion() {
		
		boolean allMessagesPushedForConversion = true;
		
		String uploadId = topic.split("_")[1];
		ConversionBean uploadBean = conversionDao.getUploadBeansFromId(uploadId);
		if(uploadBean != null)
			allMessagesPushedForConversion = uploadBean.isAllMessagesPushedForConversion();
		logger.info("allMessagesPushedForConversion is {} for topic {}", allMessagesPushedForConversion, topic);
		
		return allMessagesPushedForConversion;
		
	}
	
	public void closeConsumerOnBatchCompletion() {
		
		updateUploadBean();
		consumer.close();
		
	}

	private void updateUploadBean() {
		
		PreprocessingBean uploadBean = preprocessingDao.getUploadBeansFromId(topic.split("_")[1]);
		uploadBean.setAllMessagesPushedForPreprocessing(true);
		preprocessingDao.save(uploadBean);
		
		ConversionBean conversionBean = conversionDao.getUploadBeansFromId(topic.split("_")[1]);
		if(conversionBean != null)
			conversionDao.delete(conversionBean);
		
	}
	
	public Consumer<Long, String> getConsumer(){
		
		return consumer;
		
	}
	
}
