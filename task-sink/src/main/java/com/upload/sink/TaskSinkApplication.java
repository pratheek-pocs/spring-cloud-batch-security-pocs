package com.upload.sink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

	
@SpringBootApplication
@Import({org.springframework.cloud.stream.app.task.launcher.dataflow.sink.TaskLauncherDataflowSinkConfiguration.class})
public class TaskSinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskSinkApplication.class, args);
	}
}
